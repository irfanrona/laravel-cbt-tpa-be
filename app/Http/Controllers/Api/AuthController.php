<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    //login
    public function login(Request $request)
    {
        //validate request
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        //check if user exist
        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response([
                'message' => ['These credentials do not match our records.']
            ], 404);
        }

        //generate token
        $token = $user->createToken('auth-token')->plainTextToken;

        //return response
        return response([
            'status' => 'success',
            'token' => $token,
            'user' => $user
        ], 200);
    }

    //logout
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Logout successfully'
        ], 200);
    }
}
