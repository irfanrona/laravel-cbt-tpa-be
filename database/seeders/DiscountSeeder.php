<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //manual input
        \App\Models\Discount::create([
            'name' => 'Discount Member',
            'description' => 'Discount Member 1',
            'type' => 'percentage',
            'value' => 20,
            'status' => 'active',
            'expired_date' => '2025-01-01',
        ]);

        \App\Models\Discount::create([
            'name' => 'Taun Baru',
            'description' => 'New Years Discount',
            'type' => 'percentage',
            'value' => 10,
            'status' => 'active',
            'expired_date' => '2025-01-01',
        ]);

        \App\Models\Discount::create([
            'name' => 'Maljum',
            'description' => 'Diskon Malam Jumat',
            'type' => 'percentage',
            'value' => 15,
            'status' => 'active',
            'expired_date' => '2025-01-01',
        ]);
    }
}
